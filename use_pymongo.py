import pymongo,os,yaml,json
base = os.path.dirname(os.path.abspath(__file__))

class MongoDB:
    '''
    '''
    def __init__(self, ip="127.0.0.1", port=27017):
        '''
        args:\n
        -ip  (str) \n
          Connect IP Address\n
        -port (int)\n
          Connect Port No\n
        '''
        client          = pymongo.MongoClient(host=ip, port=port)
        self.Client     = client
        self.IP         = ip
        self.PortNo     = port

    # Property
    def DataBaseNames(self):
        '''
        Return Database Names\n

        Args:\n
          None\n
        Return:\n
        -DatabaseNames(List(str))\n
          DatabaseNames
        '''
        # return self.Client.database_names
        return self.Client.list_database_names()

    def CollectionNames(self, DataBaseName):
        return self.Client[DataBaseName].collection_names()

    def GetDocmentKeysOfFirst(self, DataBaseName, CollectionName):
            return self.Client[DataBaseName][CollectionName].find_one().keys()


    # Find系
    def FindAllDocuments(self, DataBaseName, CollectionName, query =None):
        if (query == None) or (isinstance(query, dict)):
            datas = self.Client[DataBaseName][CollectionName].find(query)
            return [i for i in datas]
        else:
            return False

    def FindOneDocuments(self, DataBaseName, CollectionName, query =None):
        if (query == None) or (isinstance(query, dict)):
            return self.Client[DataBaseName][CollectionName].find_one(query)
        else:
            return False
        
    
    # Add系
    def AddDocument(self, DataBaseName, CollectionName, Document):
        if (isinstance(Document, list) == True) and (isinstance(Document[0], dict)):
            self.Client[DataBaseName][CollectionName].insert_many(Document)
            return True
        elif isinstance(Document, dict):
            self.Client[DataBaseName][CollectionName].insert_one(Document)
            return True
        else:
            return False

    # Update系
    def ReplaseDocument(self, DataBaseName, CollectionName, Document):
        if isinstance(Document, dict):
            self.Client.get_database(DataBaseName).get_collection(CollectionName).save(Document)

        else:
            return False

    # Delete系
    def DeleteDatabase(self, DataBaseName):
        self.Client.drop_database(DataBaseName)

        return True

    def DeleteCollection(self, DataBaseName, CollectionName):
        self.Client.get_database(DataBaseName).drop_collection(CollectionName)
        # collection = self.Client.get_database(DataBaseName).get_collection(CollectionName)
        # collection.drop()

    def DeleteOneDocment(self, DataBaseName, CollectionName, query = {}):
        if (query == None) or (isinstance(query, dict)):
            collection = self.Client.get_database(DataBaseName).get_collection(CollectionName)
            count = collection.delete_one(query)
            if count == 1:
                return True
            elif count > 1:
                return count
            else:
                return False
        else:
            return False

    def DeleteManyDocment(self, DataBaseName, CollectionName, query = {}):
        if (query == None) or (isinstance(query, dict)):
            collection = self.Client.get_database(DataBaseName).get_collection(CollectionName)
            result = collection.delete_many(query)
            count  = result.deleted_count
            if count > 0:
                return count
            else:
                return False
        else:
            return False
